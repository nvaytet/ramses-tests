#!/bin/bash

# Extract time from log
s=$(grep Fine log | tail -n 1);
a=( $s );
echo ${a[4]} > time.dat;

# Extract data from binary files
../visu/amr2map -inp output_00001 -out data1.dat -typ 1 -fil ascii;
../visu/amr2map -inp output_00003 -out data2.dat -typ 1 -fil ascii;
../visu/amr2map -inp output_00004 -out data3.dat -typ 1 -fil ascii;
../visu/amr2map -inp output_00005 -out data4.dat -typ 1 -fil ascii;
../visu/amr2map -inp output_00006 -out data5.dat -typ 1 -fil ascii;
../visu/amr2map -inp output_00006 -out data6.dat -typ 0 -fil ascii -max 1;

cat data[1-6].dat > data.dat
