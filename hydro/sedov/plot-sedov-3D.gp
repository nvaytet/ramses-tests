reset

file1 = 'data1.dat'
file2 = 'data2.dat'
file3 = 'data3.dat'
file4 = 'data4.dat'
file5 = 'data5.dat'
file6 = 'data6.dat'

set term post enh color landscape
set output 'sedov-3D.ps'

set xlabel 'Distance x (cm)'
set ylabel 'Distance y (cm)'

set multiplot layout 2,3

unset key
set view map
set size square

set cblabel 'Density'
splot file1 u 1:2:3 w image
splot file2 u 1:2:3 w image
splot file3 u 1:2:3 w image
splot file4 u 1:2:3 w image
splot file5 u 1:2:3 w image
set cblabel 'AMR level'
splot file6 u 1:2:3 w image

unset multiplot

unset output
