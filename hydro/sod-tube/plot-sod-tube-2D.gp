reset

lwidth = 2

file1 = 'data1.dat'
file2 = 'data2.dat'
file3 = 'data3.dat'
file4 = 'data4.dat'
file5 = 'sod-tube-ana.dat'

lmin = int(system(sprintf("grep levelmin sod-tube-2D.nml | cut -d '=' -f2")))
lmax = int(system(sprintf("grep levelmax sod-tube-2D.nml | cut -d '=' -f2")))

set term post enh color portrait
set output 'sod-tube-2D.ps'

set xlabel 'Distance y (cm)'

nxpanel=1
nypanel=3
panlgap=0.08

marginx1=0.10
marginx2=0.90
marginy1=0.07
marginy2=0.99

dxpanel=(marginx2-marginx1-(nxpanel-1)*panlgap)/nxpanel
dypanel=(marginy2-marginy1-(nypanel-1)*panlgap)/nypanel

set multiplot
unset key

set lmargin at screen marginx1
set rmargin at screen marginx2
set bmargin at screen (marginy2-dypanel)
set tmargin at screen marginy2

set y2label 'AMR level' offset -2.0
set ytics nomirror
set y2tics
set autoscale  y
set y2tics lmin,1,lmax

set ylabel 'Density (g/cm3)' offset 2.0
plot file1 u 2:3 lc 9 lt 4 axes x1y2, file2 u 2:3 lc 0 lt 6, file5 u 2:4 w l lw lwidth lt 1

unset y2label
unset y2tics

set bmargin at screen (marginy1+dypanel+panlgap)
set tmargin at screen (marginy2-dypanel-panlgap)

set ylabel 'Velocity (cm/s)' offset 2.0
plot file3 u 2:3 lc 0 lt 6, file5 u 2:3 w l lw lwidth lt 1

set bmargin at screen marginy1
set tmargin at screen (marginy1+dypanel)

set ylabel 'Pressure (g/cm/s2)' offset 2.0
plot file4 u 2:3 lc 0 lt 6, file5 u 2:5 w l lw lwidth lt 1

unset multiplot

unset output
