#!/bin/bash

# Extract time from log
s=$(grep Fine log | tail -n 1);
a=( $s );
echo ${a[4]} > time.dat;

# Extract data from binary files
../../visu/amr2map -inp output_00002 -out data1.dat -typ 0 -fil ascii -xmi 0.498 -xma 0.502 -max 1;
../../visu/amr2map -inp output_00002 -out data2.dat -typ 1 -fil ascii -xmi 0.498 -xma 0.502;
../../visu/amr2map -inp output_00002 -out data3.dat -typ 3 -fil ascii -xmi 0.498 -xma 0.502;
../../visu/amr2map -inp output_00002 -out data4.dat -typ 4 -fil ascii -xmi 0.498 -xma 0.502;

cat data1.dat data2.dat data3.dat data4.dat > data.dat
