#!/bin/bash

# Extract time from log
s=$(grep Fine log | tail -n 1);
a=( $s );
echo ${a[4]} > time.dat;

# Extract data from binary files
../../visu/amr2cube -inp output_00002 -out data1.dat -typ 0 -fil txt -xmi 0.495 -xma 0.505 -ymi 0.495 -yma 0.505 -max 1;
../../visu/amr2cube -inp output_00002 -out data2.dat -typ 1 -fil txt -xmi 0.495 -xma 0.505 -ymi 0.495 -yma 0.505;
../../visu/amr2cube -inp output_00002 -out data3.dat -typ 4 -fil txt -xmi 0.495 -xma 0.505 -ymi 0.495 -yma 0.505;
../../visu/amr2cube -inp output_00002 -out data4.dat -typ 5 -fil txt -xmi 0.495 -xma 0.505 -ymi 0.495 -yma 0.505;

cat data1.dat data2.dat data3.dat data4.dat > data.dat
