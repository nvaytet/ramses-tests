reset

file1 = 'data1.dat'

set term post enh color landscape size 9in,9in
set output 'orszag-tang-2D.ps'

set size square
set view map
set xlabel 'Distance x'
set ylabel 'Distance y'
set cblabel 'Density'
unset key
splot file1 u 1:2:3 w image

unset output
