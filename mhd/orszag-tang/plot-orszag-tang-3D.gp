reset

file1 = 'data1.dat'
file2 = 'data2.dat'
file3 = 'data3.dat'

set term post enh color landscape
set output 'orszag-tang-3D.ps'

set multiplot layout 1,3

set size square
set view map
set cblabel 'Density'
set colorbox horiz
set colorbox user origin graph 0.0,1.05 size graph 1.0,0.07
set cbtics offset graph 0,0.25
set cblabel offset graph 0,0.53
unset key

set xlabel 'Distance x'
set ylabel 'Distance y'
splot file1 u 1:2:4 w image
set xlabel 'Distance x'
set ylabel 'Distance z'
splot file2 u 1:3:4 w image
set xlabel 'Distance y'
set ylabel 'Distance z'
splot file3 u 2:3:4 w image

unset multiplot

unset output
