#!/bin/bash


../../visu/amr2cube -inp output_00002 -out data1.dat -typ 1 -fil txt -zmi 0.495 -zma 0.505;
../../visu/amr2cube -inp output_00002 -out data2.dat -typ 1 -fil txt -ymi 0.495 -yma 0.505;
../../visu/amr2cube -inp output_00002 -out data3.dat -typ 1 -fil txt -xmi 0.495 -xma 0.505;

echo $(md5sum data1.dat | cut -d ' ' -f 1) > data.dat

exit;
